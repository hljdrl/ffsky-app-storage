package com.ffsky.template.demo;

import com.ffsky.template.demo.data.MyLinkSource;
import com.gitee.hljdrl.ffkit.FFKitApplication;
import com.gitee.hljdrl.ffkit.builder.FFKitLinkSource;
import com.gitee.hljdrl.storage.Storage;

public class MyApplication extends FFKitApplication {


    @Override
    public void onCreate() {
        super.onCreate();
        Storage.install(this);
    }

    @Override
    public Class<? extends FFKitLinkSource> getLinkSource() {
        return MyLinkSource.class;
    }
}
