package com.ffsky.template.demo.data;


import android.app.Activity;
import android.widget.Toast;

import com.gitee.hljdrl.ffkit.app.FFKitAboutActivity;
import com.gitee.hljdrl.ffkit.bean.FFKitLink;
import com.gitee.hljdrl.ffkit.builder.FFKitLinkSource;
import com.gitee.hljdrl.ffkit.fragment.FFKitColorFragment;
import com.gitee.hljdrl.ffkit.listener.OnExecuteListener;
import com.gitee.hljdrl.storage.Storage;
import com.gitee.hljdrl.storage.basis.StorageEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyLinkSource extends FFKitLinkSource {


    @Override
    public boolean isOverlayList() {
        return true;
    }

    @Override
    public List<FFKitLink> getList() {
        List<FFKitLink> list = new ArrayList<>();
        //----------------------------------------------------------------

        list.add(new FFKitLink.Builder().setName("Storage init").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                //数据存储全路径,默认存储区/android/data/{pkg},如果没有外置存储则在/data/{pkg}/
                String sdkStorageRoot = null;
                Storage.getInstance().init(sdkStorageRoot);
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("logger file").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                //数据存储全路径,默认存储区/android/data/{pkg},如果没有外置存储则在/data/{pkg}/
                String appLogger = Storage.getInstance().getWritePath("app_logger.log", StorageEvent.FILE_LOG);
                Toast.makeText(activity, appLogger, Toast.LENGTH_SHORT).show();
                File file = new File(appLogger);
                if(file.exists()){
                    //TODO
                }else{
                    //TODO
                }

            }
        }).build());

        list.add(new FFKitLink.Builder().setName("Hello World! Activity").setActivityClass(FFKitAboutActivity.class).build());
        //----------------------------------------------------------------
        list.add(new FFKitLink.Builder().setName("Hello World! Fragment").setFragmentClass(FFKitColorFragment.class).build());
        //----------------------------------------------------------------
        list.add(new FFKitLink.Builder().setName("Hello World! Fragment List").addFragmentClass(FFKitColorFragment.class, FFKitColorFragment.class, FFKitColorFragment.class, FFKitColorFragment.class).build());
        //----------------------------------------------------------------
        Storage.getInstance().init(null);
        return list;
    }


    @Override
    public String getAppName() {
        return "storage app";
    }
}
