package com.gitee.hljdrl.storage;

import android.content.Context;

import com.gitee.hljdrl.storage.basis.StorageEvent;

public class Storage extends ExtStorage {

    private static StorageEvent INSTANCE;

    public static void install(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new Storage(context);
        }
    }

    synchronized public static StorageEvent getInstance() {
        return INSTANCE;
    }

    public Storage(Context context) {
        super(context);
    }
}
