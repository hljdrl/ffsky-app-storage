package com.gitee.hljdrl.storage.basis;

import java.io.File;
import java.util.List;

public interface StorageEvent {

    String TAG = "StorageEvent";

//    long K = 1024;
//    long M = 1024 * 1024;
//    // 外置存储卡默认预警临界值
//    long THRESHOLD_WARNING_SPACE = 100 * M;
//    // 保存文件时所需的最小空间的默认值
//    long THRESHOLD_MIN_SPACE = 20 * M;

    String FILE_LOG = "log";

    String FILE_TEMP = "temp";

    String FILE_IMAGE = "image";

    String FILE_THUMB = "thumb";

    String NO_MEDIA_FILE_NAME = ".nomedia";

    void init(String sdkStorageRoot);

    List<String> listDirectory();

    void addDir(String dirName);

    void checkStorageValid();

    String getReadPath(String fileName, String fileType);

    String getWritePath(String fileName, String fileType);

    String getDirectoryByDirType(String dir);

    long getAvailableExternalSize();


    boolean isExternalStorageExist();

    File getFamilyFile();
}
