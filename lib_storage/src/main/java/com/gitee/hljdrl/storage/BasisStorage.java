package com.gitee.hljdrl.storage;

import android.content.Context;
import android.text.TextUtils;

import com.gitee.hljdrl.storage.basis.StorageEvent;

import java.util.ArrayList;
import java.util.List;

abstract class BasisStorage implements StorageEvent {

    protected Context mContext;

    protected List<String> mDirs = new ArrayList<>();

    public BasisStorage(Context context) {
        this.mContext = context;
    }

    @Override
    public void addDir(String dirName) {
        if (!TextUtils.isEmpty(dirName)) {
            mDirs.add(dirName);
        }
    }

    @Override
    public List<String> listDirectory() {
        List<String> dirs = new ArrayList<>();
        if (mDirs != null && !mDirs.isEmpty()) {
            dirs.addAll(mDirs);
        }
        dirs.add(FILE_IMAGE);
        dirs.add(FILE_LOG);
        dirs.add(FILE_TEMP);
        dirs.add(FILE_THUMB);
        //
        return dirs;
    }

    public final Context getContext() {
        return mContext;
    }


    protected String string(String... str) {
        StringBuffer buffer = new StringBuffer();
        if (str != null) {
            for (String _s : str) {
                buffer.append(_s);
            }
        }
        return buffer.toString();
    }

    protected String string(Object... str) {
        StringBuffer buffer = new StringBuffer();
        if (str != null) {
            for (Object _s : str) {
                if (_s != null) {
                    buffer.append(_s);
                }
            }
        }
        return buffer.toString();
    }

}
