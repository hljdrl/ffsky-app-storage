package com.gitee.hljdrl.storage.util;

import android.os.StatFs;

public class StorageUtil {

    private static long formatSize(long size) {
        long _tempSize = size;
        long _formatSize = size;
        if (_tempSize > 1024) {
            _tempSize /= 1024;
            if (_tempSize > 1024) {
                _tempSize /= 1024;
                _formatSize = _tempSize;
            } else {
                //0-MB
                _formatSize = 0;
            }
        } else {
            //0-MB
            _formatSize = 0;
        }
        return _formatSize;
    }

    public static boolean checkSizeEnable(String path, int value) {
        StatFs sf = new StatFs(path);//创建StatFs对象
        long blockSize = sf.getBlockSize();//获得blockSize
        long totalBlock = sf.getBlockCount();//获得全部block
        long availableBlock = sf.getAvailableBlocks();//获取可用的block
        //用String数组来存放Block信息
        //总量
        long total = formatSize(totalBlock * blockSize);
        //剩余空间大小
        long available = formatSize(availableBlock * blockSize);
        //在ProgressBar中显示可用空间的大小
        if (available > value) {
            return true;
        }
        return false;
    }

    /**
     * 获取目录剩余空间
     *
     * @param directoryPath
     */
    public static long getResidualSpace(String directoryPath) {
        try {
            StatFs sf = new StatFs(directoryPath);
            long blockSize = sf.getBlockSize();
            long availCount = sf.getAvailableBlocks();
            long availCountByte = availCount * blockSize;
            return availCountByte;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
