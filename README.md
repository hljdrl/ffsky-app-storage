#  ffsky-app-storage

#### 介绍
app file存储目录组件

接口层API StorageEvent.java

|                       方法                         |          备注              |
|----------------------------------------------------|---------------------------|
|  init(String sdkStorageRoot)                       |   初始化                   |
|  List<String> listDirectory()                      |   获取要创建的子文件夹       |
|  addDir(String dirName)                            |   添加自定义文件夹          |
|  checkStorageValid                                 |   检查存储是否创建目录文件夹  |
|  getReadPath(String fileName, String fileType)     |   获取一个可读取文件的全路径  |
|   getWritePath(String fileName, String fileType)   |   获取一个写入文件的全路径   |
|  getDirectoryByDirType(String dir)                 |   获取指定类型的文件路径     |
|  getAvailableExternalSize                          |   存储可用空间             |
|  isExternalStorageExist                            |   扩展存储是否存在          |
|  getFamilyFile                                     |   当前存储目录              |

#### 使用说明

1.  引入库

```gradle
api "com.gitee.hljdrl:storage:1.0.0"
```

2.  初始化

```java

public class MyApplication extends FFKitApplication {


    @Override
    public void onCreate() {
        super.onCreate();
        //不会创建任何文件
        Storage.install(this);
    }

}


```

3.  隐私协议同意之后初始化

```java

  //数据存储全路径,默认存储区/android/data/{pkg},如果没有外置存储则在/data/{pkg}/
  String sdkStorageRoot=null;
  Storage.getInstance().init(sdkStorageRoot);

```

4. 获取文件地址

```java
//数据存储全路径,默认存储区/android/data/{pkg},如果没有外置存储则在/data/{pkg}/
  String appLogger = Storage.getInstance().getWritePath("app_logger.log", StorageEvent.FILE_LOG);
  Toast.makeText(activity, appLogger, Toast.LENGTH_SHORT).show();
  File file = new File(appLogger);
 if(file.exists()){
    //TODO
   }else{
    //TODO
 }

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


